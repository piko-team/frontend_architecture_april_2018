const express = require('express');
const bodyParser = require('body-parser');

const auth = require('./auth');
const compression = require('compression');


module.exports = function (pubsub) {
    const app = express();
    app.use(compression());
    app.use(bodyParser.json());
    app.use(express.static('frontend'));
    const postRoutes = require('./postRoutes')(pubsub);
    const userRoutes = require('./userRoutes');

    app.get('/api/post', postRoutes.getLastPosts);
    app.post('/api/post', postRoutes.createPost);
    app.get('/api/event/post', postRoutes.broadcastPost);

    app.post('/api/user', userRoutes.register);
    app.post('/api/session', userRoutes.login);
    app.get('/api/user', userRoutes.getInfo);

    app.get('/:page*?', function serveEntryPage(req, res) {
        res.sendFile('index.html', {'root': './frontend'});
    });

    app.use(function clientError(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    app.use(function serverError(err, req, res, next) {
        console.error(err.stack);
        res.status(err.status || 500);
        res.json({message: err.message, error: (process.env.NODE_ENV === 'production') ? {} : err.stack});
    });

    return app;
};
