const jwt = require('jsonwebtoken');
const config = require('./config');

module.exports = function(req, res, next) {
    const token = req.headers['x-auth'];
    if (token) {
        try {
            jwt.verify(token, config.secret);
            next();
        } catch (e) {
            console.error(e);
            res.status(401).json({ error: 'Token verification failed' });
        }

    } else {
        res.status(401).json({ error: 'No token provided' });
    }
};