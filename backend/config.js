module.exports = {
    port: process.env.PORT || 3000,
    secret: process.env.SECRET || 'supersecretkey',
    db: process.env.MONGODB_URI || 'mongodb://localhost:27017/socialapp'
};