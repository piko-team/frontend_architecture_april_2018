var db = require('./db');
var Post = db.model('Post', {
    username: {type: String, required: true, minlength: 1, maxlength: 20},
    body: {type: String, required: true, minlength: 1, maxlength: 140},
    id: {type: String, required: false, minlength: 36, maxlength: 36},
    date: {type: Date, required: true, default: Date.now}
});
module.exports = Post;