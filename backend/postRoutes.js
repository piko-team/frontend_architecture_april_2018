const Post = require('./post');

module.exports = function (pubsub) {
    return {
        getLastPosts(req, res, next) {
            Post.find()
                .sort('-date')
                .limit(10)
                .exec(function (err, posts) {
                    if (err) {
                        return next(err);
                    }
                    res.json(posts);
                });
        },
        createPost(req, res, next) {
            var post = new Post({
                username: req.body.username,
                body: req.body.body,
                id: req.body.id
            });
            post.save(function (err, post) {
                if (err) {
                    return next(err);
                }
                pubsub.emit('post', post);
                res.status(201).json(post);
            });
        },
        broadcastPost(req, res, next) {
            res.writeHead(200, {
                'Content-Type': 'text/event-stream',
                'Cache-Control': 'no-cache',
                'Connection': 'keep-alive'
            });

            function listener(post) {
                // res.write('id: ' + new Date().getTime() + '\n');
                // res.write('event: post\n');
                res.write('data: ' + JSON.stringify(post) + '\n\n');
                res.flush();
            }

            pubsub.on('post', listener);

            // comment this out to get a memory leak
            res.on('close', function () {
                pubsub.removeListener('post', listener);
            });
        }
    }
};