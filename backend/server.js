const config = require('./config');
const EventEmitter = require('events');
var pubsub = new EventEmitter(); // replace with redis when more than one server
const app = require('./app')(pubsub);
const http = require('http').Server(app);
require("./wss")(http, pubsub);

const port = config.port;
http.listen(port, function() {
    console.log(`Example app listening on port ${port}!`);
});