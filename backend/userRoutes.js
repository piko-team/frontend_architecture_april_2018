const User = require('./user');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const config = require('./config');

module.exports = {
    register (req, res, next) {
        var user = new User({username: req.body.username});
        bcrypt.hash(req.body.password, null, null, function (err, hash) {
            user.password = hash;
            user.save(function (err) {
                if (err) {
                    return next(err);
                }
                res.sendStatus(201);
            })
        });
    },
    login (req, res, next) {

        console.log(JSON.stringify(req.body));
        // TODO: prevent duplicates
        User.findOne({username: req.body.username})
            .select('password')
            .exec(function (err, user) {
                console.log(user);
                console.log(JSON.stringify(user));
                if (err) {
                    return next(err);
                }
                if (!user) {
                    return res.sendStatus(401);
                }
                bcrypt.compare(req.body.password, user.password, function (err, valid) {
                    if (err) {
                        return next(err);
                    }
                    if (!valid) {
                        return res.sendStatus(401);
                    }
                    var token = jwt.sign({username: req.body.username}, config.secret, {expiresIn: '1h'});
                    res.json(token);
                })
            });
    },
    getInfo (req, res, next) {
        const token = req.headers['x-auth'];
        try {
            var auth = jwt.verify(token, config.secret);
        } catch (err) {
            next(err);
        }
        User.findOne({username: auth.username}, function (err, user) {
            res.json(user);
        })
    }
};