const WebSocketServer = require('ws').Server;

module.exports = function wss(httpServer, pubsub) {
    const wss = new WebSocketServer({server: httpServer});

    pubsub.on('post', function broadcast(post) {
        wss.clients.forEach(function(client) {
            client.send(JSON.stringify(post));
        });
    });

    return wss;
};