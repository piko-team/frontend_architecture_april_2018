import { h, app } from 'hyperapp';
import { Switch, Route, location } from '@hyperapp/router';
import { withLogger } from '@hyperapp/logger';
import { withPersist } from './persiste';
import layout from './view/layout';
import postModule from './post';
import userModule from './user';
import postList from './view/postList';
import loginForm from './view/login';

const state = {
    location: location.state,
    ...userModule.state,
    ...postModule.state,
};

const actions = {        
    location: location.actions,
    ...userModule.actions,    
    ...postModule.actions,
};


const view = (state, actions) =>
<div>
    {layout(state, actions.logout)}
    <Switch>
        <Route path="/" render={() => postList(state, actions) } />
        <Route path="/register" render={() => <div>Register</div>} />
        <Route path="/login" render={() => loginForm(state, actions) } />
        <Route render={() => <div>Not found</div>} />
    </Switch>
</div>


const myApp = withLogger(withPersist)(app)(state, actions, view, document.body);
myApp.loadPosts();
const unsubscribe = location.subscribe(myApp.location);
