export function withPersist(app) {
    return function(state, actions, view, container) {

        function save(state) {
            try {
                localStorage.setItem('hyperapp-persist', JSON.stringify(state));
            } catch(e) {
                console.log(e);
            }
        }

        function load() {
            try {
                return JSON.parse(localStorage.getItem('hyperapp-persist')) || {};
            } catch(e) {
                return {};
            }
        }

        const newState = Object.assign({}, state, load());
        const newActions = Object.assign({}, actions, {
            __save: () => (state, actions) => save(state)
        });

        const main = app(newState, newActions, view, container);

        window.addEventListener('unload', function() {
            main.__save();
        });

        return main;
    }
}
