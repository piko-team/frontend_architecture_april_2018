import postGatewayFactory from './postGateway';
const postGateway = postGatewayFactory(fetch);

const api = {
    state: {
        posts: [],
        currentPostText: ''
    },
    actions: {
        addPost: () => prevState => {
            const post = {username: prevState.loggedInUsername, body: prevState.currentPostText};
    
            return prevState.currentPostText.trim().length > 0 ?
                (function() { 
                    postGateway.save(post, prevState.token);
    
                    return Object.assign({}, prevState, {
                        posts: [
                            post
                        ].concat(prevState.posts),
                        currentPostText: ''
                    });
                })()
            : prevState
        },
        updatePostText: (currentPostText) => prevState => Object.assign({}, prevState, {currentPostText}),
        setPosts: posts => prevState => Object.assign({}, prevState, {posts}),
        loadPosts: () => (prevState, actions) => {
            postGateway.loadLatest()
                .then(actions.setPosts)
                .catch(console.log);
        },
        cleanAll: () => prevState => Object.assign({}, prevState, {
            posts: []
        }),
        removeLast: () => prevState => Object.assign({}, prevState, {
            posts: prevState.posts.slice(0, prevState.posts.length -1),
        }),
    }
};

export default api;