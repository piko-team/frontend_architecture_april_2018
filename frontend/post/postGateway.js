export default function postGateway(fetch) {
    return {
        save(post, token) {
            return fetch('/api/post', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                    'X-Auth': token
                },
                body: JSON.stringify(post)
            });
        },
        loadLatest() {
            return fetch('/api/post')
                .then(response => response.json());
        }
    };
};