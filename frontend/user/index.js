import userGatewayFactory from './userGateway';

const userGateway = userGatewayFactory(fetch);

const api = {
    state: {
        username: '',
        password: '',
        loggedInUsername: '',
    },
    actions: {
        setLogin: username => prevState => Object.assign({}, prevState, {username}),
        setPassword: password => prevState => Object.assign({}, prevState, {password}),

        submitLogin: () => (prevState, actions) => {
            userGateway.login(prevState.username, prevState.password)
                .then(token => {
                    actions.setLogInUser(token);
                    actions.resetCredentials();
                    actions.location.go('/#');
                })
                .catch(e=>{
                    actions.resetPassword();
                });
        },

        setLogInUser: token => prevState => Object.assign({}, prevState, {token, loggedInUsername: prevState.username}),
        resetCredentials: () => prevState => Object.assign({}, prevState, {username: '', password: ''}),
        resetPassword: () => prevState => Object.assign({}, prevState, {password: ''}),
        logout: () => prevState => Object.assign({}, prevState, {loggedInUsername: '', token: ''}),
    }
};

export default api;