export default function userGateway(fetch) {
    return {
        login(username, password) {
            const authenticationsPair = {username, password};
            return fetch('/api/session', {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(authenticationsPair)
            }).then(function (response) {
                if(response.status === 200) {
                    return response.json();
                } else {
                    return Promise.reject(response.statusText);
                }
            });
        }
    };
};