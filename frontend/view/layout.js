import { h } from 'hyperapp';
import { Link } from '@hyperapp/router';

const loggedInUserView = (username, logoutDelegate) => 
    <p class="navbar-text navbar-right" onclick={logoutDelegate} >
        Signed in as {username}
    </p>
;

const layout = (state, logoutDelegate) =>
    <nav class='navbar navbar-default'>
        <div class='container'>
            <ul class='nav navbar-nav'>
                <li><Link to='/#'>Posts</Link></li>
                <li><Link to='/register#'>Register</Link></li>
                <li><Link to='/login#'>Login</Link></li>
            </ul>
            {state.loggedInUsername ? loggedInUserView(state.loggedInUsername, logoutDelegate) : ''}
        </div>
    </nav>
;

export default layout;