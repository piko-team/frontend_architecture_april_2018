import { h } from 'hyperapp';

const loginForm = (state, actions) => 
    <div class='container'>
        <form class='container'>
            <div class="form-group">
                <label>Username</label>
                <input class="form-control" type="text" oninput={e => actions.setLogin(e.target.value) } value={state.username} />
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="form-control" type="password" oninput={e => actions.setPassword(e.target.value) } value={state.password} />
            </div>
            <input class="btn btn-success btn-lg btn-block" value="Login" onclick={e => {e.preventDefault(); actions.submitLogin(); }} />
        </form>
    </div>
;

export default loginForm;