import { h } from 'hyperapp';

const listItem = post =>
    (<li class='list-group-item'>
        <strong>@{post.username}</strong>
        <span> {post.body}</span>
    </li>)
;

const postList = (state, actions) =>
    <div class='container'>
        <h1>Recent Posts</h1>
        {
            state.token ? 
                <button class='btn btn-default' onclick={actions.addPost}>Add Post</button>
            : ''
        }
        <button class='btn btn-default' onclick={actions.cleanAll}>Clean all</button>
        <button class='btn btn-default' onclick={actions.removeLast}>Remove last item</button>

        <input oninput={e => actions.updatePostText(e.target.value)} value={state.currentPostText} />
        
        <ul class='list-group'>
            {state.posts.map(listItem)}
        </ul>
    </div>
;

export default postList;