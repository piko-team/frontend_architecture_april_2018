const path = require("path");
const webpack = require("webpack");

module.exports = {
    entry: "./frontend/app.js",
    output: {
        path: path.join(__dirname, "frontend"),
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: "babel-loader"
            }
        ]
    }
};